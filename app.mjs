/**
 * @param value any
 * @return {any}
 * @constructor
 */
const Right = value => {
    if (value?.isMonoid === undefined)
        return {
            /**
             * @param {function(*): * | Right | Left} fn
             * @return {Right}
             */
            map: fn=>Right(fn(value)),
            /**
             * @return {Right}
             */
            flatten: ()=>Right(value.value),
            /**
             * @return {Right}
             */
            catch: ()=>Right(value),
            isMonoid: true,
            value,
        };
    else
        return {
            /**
             * @param {function(*): *} fn
             * @return {Right}
             */
            map: fn=>Right(fn(value.value)),
            /**
             * @return {Right}
             */
            flatten: ()=>Right(value.value),
            /**
             * @return {Right}
             */
            catch: ()=>Right(value.value),
            isMonoid: true,
            value: value.value,
        };
}
/**
 * @param value any
 * @return {{flatten: (function(): Left), isMonoid: boolean true, catch: (function(function(*): *): Right), map: (function(): Left), value: *}}
 * @constructor
 */
const Left = value => ({
    /**
     * @return {{flatten: (function(): Left), isMonoid: boolean, true, catch: (function(function(*): *): Right), map: (function(): Left), value: *}}
     */
    map: ()=>Left(value),
    /**
     * @return {{flatten: (function(): Left), isMonoid: boolean, true, catch: (function(function(*): *): Right), map: (function(): Left), value: *}}
     */
    flatten: ()=>Left(value),
    /**
     * @param {function(*): *} fn
     * @return {Right}
     */
    catch: fn=>Right(fn(value)),
    isMonoid: true,
    value,
});



/**
 * @param {function(*): *} fn
 * @return {function(*=): (Right|Left)}
 */
const TryCatch = fn =>
    /**
     *
     * @param value any
     * @return {Right | Left}
     */
    value => {
        try {
            return Right(fn(value));
        } catch (error) {
            return Left(error);
        }
};

const Maybe = value => ({
    isNothing: () => value === null || value === undefined,
    map(fn){
        return this.isNothing() ? Maybe.Nothing() : Maybe(fn(value));
    },
    flatten() {
        return this.isNothing() ? Maybe.Nothing() : Maybe(value.value);
    },
    // allow to access the value or get a default one if none
    or(defaultValue) {
        return this.isNothing() ? Right(defaultValue) : Right(value);
    },
    isMonoid: true,
    value,
});

Maybe.Nothing = () => ({
    isNothing: () => true,
    map: () => Maybe.Nothing(),
    flatten: () => Maybe.Nothing(),
    or: defaultValue => defaultValue,
    value: null,
});
Right.of = value=>Right(value);
export {TryCatch, Maybe, Right};